# Handheld-Hexapod-Controller
## What is this?
	Handheld Hexapod Controller based on an arduino pro mini 5V@16MHz 

----

## General
		- Used a Arduino Pro Mini (5V@16MHz)
		- Used a ESP8266-01 via UART
		- Two generic 1$ joysticks
		- Five generic pushbuttons
		- Two 1 cell LiPo in series as power supply
		- AMS117-5 and AMS1117-3.3 for different voltage domains
----

## Display:
		Bat:xxx%   M:Walk
		Gait:Wave
		LX:xxxx   LY:xxxx
		RX:xxxx   RY:xxxx

----
## Values

	- Bat = Battery voltage in percent relative to 4.2V, where 3.0 is lowest.

	- M = Mode of the Hexapod. Walking, raising...

	- Gait = Current Walking Gait

	- LX,LY,RX,RY = Analog values of the joysticks

----
## Protocol

	- Total length of 10 bytes plus delimiter
	- Bytes 0-1: Mode bitmask
	- Bytes 2-5: Left X and Y values
	- Bytes 6-9: Right X and Y values
	- Delimiter is #

<br>
- Bitmasks
<br>

Mode|16 Bit binary  
--|--
  WALKING|0000000000000001
  RISE_BODY|0000000000000010
  WAVE_HAND|0000000000000100

Gait|16 Bit binary  
--|--
  WAVE|1000000000000000
  RIPPLE|0100000000000000
  TRIPOD|0010000000000000

----
