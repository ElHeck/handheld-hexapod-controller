#include "oled.h"
#include "Arduino.h"
#include "definitions.h"
#include "joystick.h"
#include "optionSwitches.h"
#include "espCommunication.h"

#define LEFT_STICK_X_PIN A0
#define LEFT_STICK_Y_PIN A1
#define LEFT_STICK_SWITCH_PIN 3
#define RIGHT_STICK_X_PIN A2
#define RIGHT_STICK_Y_PIN A3
#define RIGHT_STICK_SWITCH_PIN 4
#define GAIT_PLUS_BUTTON_PIN 8
#define GAIT_MINUS_BUTTON_PIN 2
#define MODE_PLUS_BUTTON_PIN 5
#define MODE_MINUS_BUTTON_PIN 7

Oled oled;
Joystick leftStick;
Joystick rightStick;
OptionSwitches switches;
buttonOptions options;
ESPInstructionSender espSender = ESPInstructionSender();
uint16_t xLeft, yLeft, xRight, yRight;

void setup() {
	delay(5000);
	espSender.begin(19200);
	oled.begin();
	oled.setVoltage(99);
	oled.setWalkingGait(WAVE);
	oled.setMode(WALKING);
	oled.setLeftY(0);
	oled.setLeftX(0);
	oled.setRightX(0);
	oled.setRightY(0);
	oled.update();

	switches.begin(GAIT_PLUS_BUTTON_PIN, GAIT_MINUS_BUTTON_PIN, MODE_PLUS_BUTTON_PIN, MODE_MINUS_BUTTON_PIN);
	leftStick.begin(LEFT_STICK_X_PIN, LEFT_STICK_Y_PIN, LEFT_STICK_SWITCH_PIN, true, true);
	rightStick.begin(RIGHT_STICK_X_PIN, RIGHT_STICK_Y_PIN, RIGHT_STICK_SWITCH_PIN, true, true);
}

void loop() {
	leftStick.readXY();
	rightStick.readXY();
	switches.update();
	options = switches.getCurrentOptions();
	xLeft = leftStick.getXValue();
	yLeft = leftStick.getYValue();
	xRight = rightStick.getXValue();
	yRight = rightStick.getYValue();
	oled.setLeftX(xLeft);
	oled.setLeftY(yLeft);
	oled.setRightX(xRight);
	oled.setRightY(yRight);
	oled.setWalkingGait(options.gait);
	oled.setMode(options.currentMode);
	oled.update();

	espSender.setModeConfiguration(options);
	espSender.setLeftJoystickValues(xLeft, yLeft);
	espSender.setRightJoystickValues(xRight, yRight);
	espSender.send();
}
