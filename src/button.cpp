#include "button.h"


void Button :: begin(switchRole role, uint8_t pin){
	ownRole = role;
	pinNumber = pin;
	oldState = DEACTIVATED;
	newState = DEACTIVATED;
	currentState = DEACTIVATED;
	lastTime = millis(); 
	pinMode(pinNumber, INPUT_PULLUP);	
	isActivated = 0;
}

switchState Button :: getCurrentState(){
	update();
	return finalState;
}

void Button :: update(){
	readNewState();
	if(oldState != newState){
		lastTime = millis();
	}
	finalState = DEACTIVATED;
	if (millis() - lastTime > DEBOUNCE_TIME_MS){
		if (newState != currentState){
			currentState = newState;
			if (currentState == ACTIVATED){
				finalState = ACTIVATED;
				isActivated = 1;
			}
		}
	}
	oldState = newState;
}

void Button :: readNewState(){
	if(digitalRead(pinNumber) == LOW){
		newState = ACTIVATED;
	}else{
		newState = DEACTIVATED;
	}
}