#ifndef ESP_COMMUNICATION_H
#define ESP_COMMUNICATION_H

#include "Arduino.h"
#include "types.h"

class  ESPInstructionSender{
private:
  uint16_t modeConfiguration;
  uint16_t xValueLeft;
  uint16_t yValueLeft;
  uint16_t xValueRight;
  uint16_t yValueRight;

public:
  ESPInstructionSender ();
  void begin(int baudRate);
  void setModeConfiguration(buttonOptions options);
  void setLeftJoystickValues(uint16_t x, uint16_t y);
  void setRightJoystickValues(uint16_t x, uint16_t y);
  void send();
  virtual ~ESPInstructionSender (){};
};


#endif
