#ifndef OPTION_SWITCHES_H
#define OPTION_SWITCHES_H

#include "Arduino.h"
#include "button.h"

class OptionSwitches
{
private:
	Button gaitPlusButton = Button();
	Button gaitMinusButton = Button();
	Button modePlusButton = Button();
	Button modeMinusButton = Button();
	buttonOptions currentOptions;

	void evaluateWalkingGaitStateMachine(switchRole role);
	void evaluateModeStateMachine(switchRole role);
	

public:
	OptionSwitches(){};
	~OptionSwitches(){};
	void begin(uint8_t gaitPlusPin, uint8_t gaitMinusPin, uint8_t modePlusPin, uint8_t modeMinusPin);
	void update();
	buttonOptions getCurrentOptions();
};


#endif
