#ifndef TYPES_H
#define TYPES_H


typedef enum{
	WAVE = 1,
	RIPPLE = 2,
	TRIPOD = 3
} walkingGait;

typedef enum{
	WALKING = 1,
	RISE_BODY = 2,
	WAVE_HAND = 3
} mode;

typedef enum{
	GAIT_PLUS = 1,
	GAIT_MINUS = 2,
	MODE_PLUS = 3,
	MODE_MINUS = 4
} switchRole;

typedef struct{
	walkingGait gait;
	mode currentMode;
} buttonOptions;

typedef enum{
	ACTIVATED = 1,
	DEACTIVATED = 2
} switchState;

#endif
