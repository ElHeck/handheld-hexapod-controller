#include "optionSwitches.h"
#include "types.h"

void OptionSwitches :: begin(uint8_t gaitPlusPin, uint8_t gaitMinusPin, uint8_t modePlusPin, uint8_t modeMinusPin){
	gaitPlusButton.begin(GAIT_PLUS, gaitPlusPin);
	gaitMinusButton.begin(GAIT_MINUS, gaitMinusPin);
	modePlusButton.begin(MODE_PLUS, modePlusPin);
	modeMinusButton.begin(MODE_MINUS, modeMinusPin);
	currentOptions.gait = WAVE;
	currentOptions.currentMode = WALKING;
}

void OptionSwitches :: update(){
	if(gaitPlusButton.getCurrentState() == ACTIVATED){
		evaluateWalkingGaitStateMachine(GAIT_PLUS);
	}
	if(gaitMinusButton.getCurrentState() == ACTIVATED){
		evaluateWalkingGaitStateMachine(GAIT_MINUS);
	}
	if(modePlusButton.getCurrentState() == ACTIVATED){
		evaluateModeStateMachine(MODE_PLUS);
	}
	if(modeMinusButton.getCurrentState() == ACTIVATED){
		evaluateModeStateMachine(MODE_MINUS);
	}
}

void OptionSwitches :: evaluateWalkingGaitStateMachine(switchRole role){
	if(role == GAIT_MINUS || role == GAIT_PLUS){
		switch(currentOptions.gait){
			case WAVE:
				if(role == GAIT_PLUS){
					currentOptions.gait = RIPPLE;
				}else if(role == GAIT_MINUS){
					currentOptions.gait = TRIPOD;
				}
				break;
			case RIPPLE:
				if(role == GAIT_PLUS){
					currentOptions.gait = TRIPOD;
				}else if(role == GAIT_MINUS){
					currentOptions.gait = WAVE;
				}
				break;
			case TRIPOD:
				if(role == GAIT_PLUS){
					currentOptions.gait = WAVE;
				}else if(role == GAIT_MINUS){
					currentOptions.gait = RIPPLE;
				}
				break;
		}
	}
}

void OptionSwitches :: evaluateModeStateMachine(switchRole role){
	if(role == MODE_PLUS || role == MODE_MINUS){
		switch(currentOptions.currentMode){
			case WALKING:
				if(role == MODE_PLUS){
					currentOptions.currentMode = RISE_BODY;
				}else if(role == MODE_MINUS){
					currentOptions.currentMode = WAVE_HAND;
				}
				break;
			case RISE_BODY:
				if(role == MODE_PLUS){
					currentOptions.currentMode = WAVE_HAND;
				}else if(role == MODE_MINUS){
					currentOptions.currentMode = WALKING;
				}
				break;
			case WAVE_HAND:
			if(role == MODE_PLUS){
					currentOptions.currentMode = WALKING;
				}else if(role == MODE_MINUS){
					currentOptions.currentMode = RISE_BODY;
				}
				break;
		}
	}
}

buttonOptions OptionSwitches :: getCurrentOptions(){
	return currentOptions;
}