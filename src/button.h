#ifndef BUTTON_H
#define BUTTON_H

#include "types.h"
#include "Arduino.h"

#define DEBOUNCE_TIME_MS 30

class Button
{

private:
	switchRole ownRole;
	uint8_t pinNumber;
	switchState oldState;
	switchState newState;
	switchState currentState;
	switchState finalState;
	unsigned long lastTime;
	uint8_t isActivated;
	void update();
	void readNewState();

public:
	Button(){};
	~Button(){};
	void begin(switchRole role, uint8_t pin);
	switchState getCurrentState();
};


#endif
