#include "joystick.h"

#define MAX_10_BIT_SCALE 1024

void Joystick :: begin(uint8_t xPin, uint8_t yPin, uint8_t switchPin, bool invertedAxis, bool switchedAxis){
	xPinNumber = xPin;
	yPinNumber = yPin;
	switchPinNumber = switchPin;
	pinMode(switchPin, INPUT);
	xValue = 0;
	yValue = 0;
	this->invertedAxis = invertedAxis;
	this->switchedAxis = switchedAxis;
}

void Joystick :: readXY(){
	if(switchedAxis){
		newXValue = analogRead(yPinNumber);
		newYValue = analogRead(xPinNumber);
	}else{
		newXValue = analogRead(xPinNumber);
		newYValue = analogRead(yPinNumber);
	}
	if(invertedAxis){
		newXValue = abs(MAX_10_BIT_SCALE - newXValue);
		newYValue = abs(MAX_10_BIT_SCALE - newYValue);
	}
}

uint8_t Joystick :: readSwitch(){
	return digitalRead(switchPinNumber);
}

uint16_t Joystick :: getXValue(){
	xValue = uint16_t(float((1 - LOW_PASS_FILTER_VALUE) * xValue) + float(LOW_PASS_FILTER_VALUE * newXValue));
	return xValue;
}

uint16_t Joystick :: getYValue(){
	yValue = uint16_t(float((1 - LOW_PASS_FILTER_VALUE) * yValue) + float(LOW_PASS_FILTER_VALUE * newYValue));
	return yValue;
}
