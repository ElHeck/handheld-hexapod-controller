#ifndef JOYSTICK_H
#define JOYSTICK_H

#include "Arduino.h"

#define LOW_PASS_FILTER_VALUE 0.4f


class Joystick
{
private:
	uint8_t xPinNumber;
	uint8_t yPinNumber;
	uint8_t switchPinNumber;
	uint16_t newXValue;
	uint16_t newYValue;
	uint16_t xValue;
	uint16_t yValue;
	bool invertedAxis;
	bool switchedAxis;

public:
	Joystick(){};
	~Joystick(){};
	void begin(uint8_t xPin, uint8_t yPin, uint8_t switchPin, bool invertedAxis, bool switchedAxis);
	void readXY();
	uint8_t readSwitch();
	uint16_t getXValue();
	uint16_t getYValue();
};
#endif
