
#ifndef OLED_H
#define OLED_H

#include "Arduino.h"
#include <Adafruit_SSD1306.h>
#include <Adafruit_GFX.h>
#include "types.h"


#define OLED_MOSI   9
#define OLED_CLK   10
#define OLED_DC    11
#define OLED_CS    12
#define OLED_RESET 13


class Oled
{
private:
	Adafruit_SSD1306 display = Adafruit_SSD1306(OLED_MOSI, OLED_CLK, OLED_DC, OLED_RESET, OLED_CS);
	uint8_t voltagePercentage;
	walkingGait currentGait;
	mode currentMode;
	uint16_t leftX;
	uint16_t leftY;
	uint16_t rightX;
	uint16_t rightY;

public:
	Oled(){};
	~Oled(){};
	void begin();
	void setVoltage(uint8_t percent);
	void setWalkingGait(walkingGait gait);
	void setMode(mode newMode);
	void setLeftX(uint16_t x);
	void setLeftY(uint16_t y);
	void setRightX(uint16_t x);
	void setRightY(uint16_t y);
	void displayVoltage();
	void displayMode();
	void displayWalkingGait();
	void displayJoystickLeftValues();
	void displayJoystickRightValues();
	void update();
};

#endif
