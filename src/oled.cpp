#include "oled.h"
#include <SPI.h>
#include <Wire.h>


void Oled :: begin(){
	display.begin(SSD1306_SWITCHCAPVCC);
	display.display();
	delay(1000);
	display.clearDisplay();
}

void Oled :: setVoltage(uint8_t percent){
	voltagePercentage = percent;
}

void Oled :: setWalkingGait(walkingGait gait){
	currentGait = gait;
}

void Oled :: setMode(mode newMode){
	currentMode = newMode;
}

void Oled :: setLeftX(uint16_t x){
	leftX = x;
}

void Oled :: setLeftY(uint16_t y){
	leftY = y;
}

void Oled :: setRightX(uint16_t x){
	rightX = x;
}

void Oled :: setRightY(uint16_t y){
	rightY = y;
}

void Oled :: displayVoltage(){
	char buffer[3];
	itoa(voltagePercentage, buffer, 10);
	display.write('B');
	display.write('A');
	display.write('T');
	display.write(':');
	display.write(buffer[0]);
	display.write(buffer[1]);
	display.write(buffer[2]);
	display.write('%');
}

void Oled :: displayMode(){
	display.write('M');
	display.write(':');
	switch(currentMode){
		case WALKING:
			display.write('W');
			display.write('A');
			display.write('L');
			display.write('K');
			break;
		case RISE_BODY:
			display.write('R');
			display.write('I');
			display.write('S');
			display.write('E');
			break;
		case WAVE_HAND:
			display.write('W');
			display.write('A');
			display.write('V');
			display.write('E');
			break;
	}
}

void Oled :: displayWalkingGait(){
	display.write('G');
	display.write('A');
	display.write('T');
	display.write('E');
	display.write(':');
	switch(currentGait){
		case WAVE:
			display.write('W');
			display.write('A');
			display.write('V');
			display.write('E');
			break;
		case RIPPLE:
			display.write('R');
			display.write('I');
			display.write('P');
			display.write('P');
			break;
		case TRIPOD:
			display.write('T');
			display.write('R');
			display.write('I');
			display.write('P');
			break;
	}
}

void Oled :: displayJoystickLeftValues(){
	char buffer[4];
	memset(buffer, 0, 4);
	itoa(leftX, buffer, 10);
	display.write('L');
	display.write('X');
	display.write(':');
	display.write(buffer[0]);
	display.write(buffer[1]);
	display.write(buffer[2]);
	display.write(buffer[3]);
	display.write(' ');
	display.write(' ');
	display.write(' ');
	display.write(' ');
	memset(buffer, 0, 4);
	itoa(leftY, buffer, 10);
	display.write('L');
	display.write('Y');
	display.write(':');
	display.write(buffer[0]);
	display.write(buffer[1]);
	display.write(buffer[2]);
	display.write(buffer[3]);
}

void Oled :: displayJoystickRightValues(){
	char buffer[4];
	memset(buffer, 0, 4);
	itoa(rightX, buffer, 10);
	display.write('R');
	display.write('X');
	display.write(':');
	display.write(buffer[0]);
	display.write(buffer[1]);
	display.write(buffer[2]);
	display.write(buffer[3]);
	display.write(' ');
	display.write(' ');
	display.write(' ');
	display.write(' ');
	memset(buffer, 0, 4);
	itoa(rightY, buffer, 10);
	display.write('R');
	display.write('Y');
	display.write(':');
	display.write(buffer[0]);
	display.write(buffer[1]);
	display.write(buffer[2]);
	display.write(buffer[3]);
}

void Oled :: update(){
	display.clearDisplay();
	display.setTextSize(1);
	display.setTextColor(WHITE);
	display.setCursor(0,0);
	displayVoltage();
	display.write(' ');
	display.write(' ');
	display.write(' ');
	displayMode();
	display.println();
	displayWalkingGait();
	display.println();
	displayJoystickLeftValues();
	display.println();
	displayJoystickRightValues();
	display.display();
}
