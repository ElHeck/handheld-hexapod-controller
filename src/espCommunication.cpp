#include "espCommunication.h"
#include "protocol.h"
#include "definitions.h"

ESPInstructionSender :: ESPInstructionSender(){
  this->modeConfiguration &= RESET;
  this->xValueLeft = 0;
  this->yValueLeft = 0;
  this->xValueRight = 0;
  this->yValueRight = 0;
}

void ESPInstructionSender :: begin(int baudRate){
  Serial.begin(baudRate);
}

void ESPInstructionSender :: setModeConfiguration(buttonOptions options){
  this->modeConfiguration &= RESET;
  switch (options.currentMode) {
    case WALKING:
      this->modeConfiguration |= (1 << MODE_WALKING);
      break;
    case RISE_BODY:
      this->modeConfiguration |= (1 << MODE_RISE_BODY);
      break;
    case WAVE_HAND:
      this->modeConfiguration |= (1 << MODE_WAVE_HAND);
      break;
  }
  switch (options.gait) {
    case WAVE:
      this->modeConfiguration |= (1 << MODE_WAVE_GAIT);
      break;
    case RIPPLE:
      this->modeConfiguration |= (1 << MODE_RIPPLE_GAIT);
      break;
    case TRIPOD:
      this->modeConfiguration |= (1 << MODE_TRIPOD_GAIT);
      break;
  }
}

void ESPInstructionSender :: setLeftJoystickValues(uint16_t x, uint16_t y){
  this->xValueLeft = x;
  this->yValueLeft = y;
}

void ESPInstructionSender :: setRightJoystickValues(uint16_t x, uint16_t y){
  this->xValueRight = x;
  this->yValueRight = y;
}

void ESPInstructionSender :: send(){
  Serial.write(this->modeConfiguration  >> BYTE_SHIFT);
  Serial.write(this->modeConfiguration & LOW_BYTE_MASK);

  Serial.write(this->xValueLeft  >> BYTE_SHIFT);
  Serial.write(this->xValueLeft & LOW_BYTE_MASK);

  Serial.write(this->yValueLeft >> BYTE_SHIFT);
  Serial.write(this->yValueLeft & LOW_BYTE_MASK);

  Serial.write(this->xValueRight  >> BYTE_SHIFT);
  Serial.write(this->xValueRight & LOW_BYTE_MASK);

  Serial.write(this->yValueRight >> BYTE_SHIFT);
  Serial.write(this->yValueRight & LOW_BYTE_MASK);

  Serial.write((uint8_t) DELIMITER);

  #ifdef SERIAL_DEBUG
    Serial.print(this->modeConfiguration);
    Serial.print("-");
    Serial.print(this->xValueLeft);
    Serial.print("-");
    Serial.print(this->yValueLeft);
    Serial.print("-");
    Serial.print(this->xValueRight);
    Serial.print("-");
    Serial.print(this->yValueRight);
    Serial.print("-");
    Serial.println("Done");
  #endif
}
