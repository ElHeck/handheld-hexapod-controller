#define DELIMITER '#'

//uint16_t protocol bits
#define RESET 0b0000000000000000
#define MODE_WALKING 0
#define MODE_RISE_BODY 1
#define MODE_WAVE_HAND 2
#define MODE_WAVE_GAIT 15
#define MODE_RIPPLE_GAIT 14
#define MODE_TRIPOD_GAIT 13
